
DROP TABLE IF EXISTS customers;
CREATE TABLE customers (
	id bigint NOT NULL AUTO_INCREMENT,
	name varchar(20) NOT NULL,
	phone varchar(20) DEFAULT NULL,
	address varchar(150) DEFAULT NULL,
	rating int DEFAULT NULL
);

INSERT INTO customers
VALUES (12, 'ООО "Интрейд лтд"', '313-48-48', 'ул. Смольная, д. 7', 1000),
	(13, 'Иванов П.В.', '336-74-87', 'пр. Пушкина, 125', 3000),
	(14, 'Петров А.Б.', NULL, NULL, 266),
	(15, 'John', NULL, NULL, 100),
	(16, 'Jane', '987654321', NULL, 124),
	(22, 'Иван', '3343-22-22', 'Садовая, 14', 123),
	(23, 'Dan', '4774747', 'tututu', 11),
	(24, 'Danis', NULL, NULL, 13),
	(25, 'Иван Иванович', NULL, NULL, NULL),
	(26, 'Maria', '123456', NULL, 10),
	(43, 'NEW CUSTOMER', '12464772634', NULL, 0),
	(45, 'Fred', '236637462', 'Some address', 35);
;

DROP TABLE IF EXISTS address;
CREATE TABLE address (
   id int NOT NULL AUTO_INCREMENT,
   address varchar(45) NOT NULL,
   customers_id bigint NOT NULL
);
INSERT INTO address
VALUES (1, 'The first address', 12),
       (2, 'The second address', 12),
       (3, 'The The first address', 13),
       (4, 'The The second address', 13);


DROP TABLE IF EXISTS products;
CREATE TABLE products (
        id bigint NOT NULL AUTO_INCREMENT,
        description text,
        details text,
        price decimal(8, 2) DEFAULT NULL,
        manual mediumblob
);

INSERT INTO products
VALUES (1, 'Обогреватель Мосбытприбор ВГД 121R', 'Инфракрасный обогреватель. 3 режима нагрева: 400 Вт, 800 Вт, 1200 Вт', 1145.00, NULL),
       (2, 'Гриль Мосбытприбор СТ-14', 'Мощность 1440 Вт. Быстрый нагрев. Термостат.\\n		Цветовой индикатор работы', 2115.00, NULL),
       (4, 'Чайник Мосбытприбор МН', 'Цвет: белый. Мощность: 2200 Вт. Объем: 2 л', 925.00, NULL),
       (5, 'Утюг Мосбытприбор c паром АБ 200', 'Цвет: фиолетовый. Мощность: 1400 вт', 518.00, '');



DROP TABLE IF EXISTS orders;
CREATE TABLE orders (
	id bigint NOT NULL AUTO_INCREMENT,
	date date DEFAULT NULL,
	product_id bigint NOT NULL,
	qty int DEFAULT NULL,
	amount decimal(10, 2) DEFAULT NULL,
	customer_id bigint DEFAULT NULL
);

INSERT INTO orders
VALUES (1, '2007-12-12', 5, 8, 4500.00
		, 12),
	(2, '2007-12-12', 2, 14, 22000.00
		, 12),
	(3, '2008-01-21', 5, 12, 5750.00
		, 13),
	(4, '2015-03-24', 4, 1, 1000.00
		, 14),
	(6, NULL, 4, 1, 11.00
		, 12),
	(7, '2005-11-11', 4, 1, 1000.00
		, 25),
	(8, '2012-12-12', 1, 1000, 1000.00
		, 26);

