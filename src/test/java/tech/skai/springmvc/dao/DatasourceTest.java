package tech.skai.springmvc.dao;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import javax.sql.DataSource;

@SpringBootTest
class DatasourceTest {
    @Autowired private DataSource dataSource;

    /**
     * Prints Datasource implementation and connection pool implementation being used.
     */
    @Test
    void datasource_implementation() {
        Assertions.assertNotNull(dataSource);
        System.out.println("DataSource: " + dataSource);
    }

}
