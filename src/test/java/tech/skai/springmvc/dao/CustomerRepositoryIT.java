package tech.skai.springmvc.dao;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.Logger;
import lombok.extern.log4j.Log4j2;
import lombok.val;
import lombok.var;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.data.jpa.repository.support.JpaEntityInformation;
import org.springframework.data.jpa.repository.support.JpaMetamodelEntityInformation;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import tech.skai.springmvc.SpringMvcExamplesApplication;
import tech.skai.springmvc.dao.base.KeysetLimitingSpecificationExecutorImpl;
import tech.skai.springmvc.dao.specifications.CustomerSpecifications;
import tech.skai.springmvc.logging.backlog.ListLogbackAppender;
import tech.skai.springmvc.model.Address;
import tech.skai.springmvc.model.Customer;
import tech.skai.springmvc.model.Order;

import javax.persistence.EntityManager;
import java.util.List;
import java.util.Optional;
import java.util.Random;

import static org.mockito.Mockito.*;

@DataJpaTest         // This loads just JPA layer
//@SpringBootTest    // This will load the whole Application Context
//@ContextConfiguration(classes = {SpringMvcExamplesApplication.class})
@Transactional(propagation = Propagation.REQUIRED, readOnly = true, noRollbackFor = Exception.class)
@ExtendWith(MockitoExtension.class)
@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
@Log4j2
class CustomerRepositoryIT {
//    @Autowired
//    private Flyway flyway;

    // Used to test the logging occurring during test execution
    @Spy ListLogbackAppender spyLogAppender = new ListLogbackAppender(); // Can also use standard ListAppender

    @TestConfiguration
    @EnableJpaRepositories(   // (!) By default searches repository classes INSIDE this package
            basePackages = {"tech.skai.springmvc.dao"},
            repositoryBaseClass = KeysetLimitingSpecificationExecutorImpl.class
    )
    static class TestConfig {
    }

    @Autowired
    CustomerRepository customerRepository;

    public CustomerRepositoryIT() {
    }

    @Test
    void findAll_produces_a_nonempty_list() {
        Iterable<Customer> customers = customerRepository.findAll();
        Assertions.assertTrue(customers.iterator().hasNext());
    }

    // Test that uses @Sql data initialization, on top of any data integration script performed at startup.
    @ParameterizedTest(name = "@Sql annotation inserted a customer id={0}, name=\'{1}\'")
    @CsvSource({"100, Test customer"})
    @Sql("/sql/salesdept_customers_insert.sql")             // Insert test data before running this test
    void sql_annotation_adds_sata(long id, String name) {
        final var customer = customerRepository.findById(id).orElse(null);
        Assertions.assertNotNull(customer);
        Assertions.assertEquals(name, customer.getName());
    }

    // Test that uses only data initialized with integration script performed at startup.
    @ParameterizedTest(name = "Customer id={0} is fetched")
    @ValueSource(longs = {12, 13, 14})
    void findById_fetches_a_customer(long id) {
        Customer customer = customerRepository.findById(id).orElse(null);
        Assertions.assertNotNull(customer);
        Assertions.assertTrue(customer.getName().length() > 0);
    }

    @ParameterizedTest(name = "Customer id={0} is absent")
    @ValueSource(longs = {1})
    void findById_returns_null_given_a_wrong_id(long id) {
        Customer customer = customerRepository.findById(id).orElse(null);
        Assertions.assertNull(customer);
    }

    @ParameterizedTest(name = "Customer id={0} has {1} addresses")
    @CsvSource({"12, 2",
                "13, 2",
                "14, 0",
                "15, 0"})
    void unidirectional_association_is_fetched(long id, int addressCount ) {
        Customer customer = customerRepository.findById(id).orElse(null);
        Assertions.assertNotNull(customer);
        List<Address> addresses = customer.getAddresses();
        Assertions.assertNotNull(addresses);
        Assertions.assertEquals(addressCount, addresses.size());
    }

    @ParameterizedTest(name = "Customer id={0} has {1} orders")
    @CsvSource({"12, 3",
            "13, 1",
            "14, 1",
            "15, 0" })
    void bidirectional_association_is_fetched(long id, int orderCount ) {
        Customer customer = customerRepository.findById(id).orElse(null);
        Assertions.assertNotNull(customer);
        Assertions.assertTrue(customer.getId() > 0);
        List<Order> orders = customer.getOrders();
        Assertions.assertNotNull(orders);
        Assertions.assertEquals(orderCount, orders.size());
        orders.forEach(o -> {
            Assertions.assertNotNull(o);
            Assertions.assertEquals(customer, o.getCustomer());
        });
    }

    @ParameterizedTest(name = "Customer id={0} is updated")
    @ValueSource(longs = {15})
    void save_updates_a_customer(long id) {
        var optCustomer = customerRepository.findById(id);
        Assertions.assertTrue(optCustomer.isPresent());
        var customer = optCustomer.get();

        val rnd = new Random();
        val phone = Integer.toString(rnd.nextInt());

        customer.setPhone(phone);
        customerRepository.save(customer);


        optCustomer = customerRepository.findById(id);
        Assertions.assertTrue(optCustomer.isPresent());
        customer = optCustomer.get();
        Assertions.assertEquals(phone, customer.getPhone());

        // In tests all transactions are rolled back, so no updates remain in DB !
    }

    @Test
    void findFirstByPhone_loads_a_single_customer() {
        // given

        // when
        Optional<Customer> optCustomer = customerRepository.findFirst(
                CustomerSpecifications.phoneEqual("987654321"),
                Sort.unsorted());

        // then
        Assertions.assertTrue(optCustomer.isPresent());
        Customer customer = optCustomer.get();
        Assertions.assertNotNull(customer.getPhone());
        Assertions.assertNotNull(customer.getName());
        Assertions.assertEquals("987654321", customer.getPhone());
        Assertions.assertEquals("Jane", customer.getName());
    }

    @Test
    void findFirstByPhone_causes_no_warnings() {
        // given
        spyLogAppender.start();
        Logger logger = (Logger) LoggerFactory.getLogger(Logger.ROOT_LOGGER_NAME);
        logger.addAppender(spyLogAppender);

        // when
        customerRepository.findFirst(
                CustomerSpecifications.phoneEqual("987654321"),
                Sort.unsorted());
        // This will produce: WARNING HHH000104: firstResult/maxResults specified with collection fetch; applying in memory!
        // Optional<Customer> optCustomer = customerRepository.findFirstByPhone("987654321");

        // then
//        List<String> list = spyLogAppender.getEvents().stream()
//                .map(ILoggingEvent::getLevel)
//                .map(Level::toString)
//                .collect(Collectors.toList());
//        System.out.println(list);
        verify(spyLogAppender, never()).doAppend(argThat(event ->
                event.getLevel().isGreaterOrEqual(Level.WARN)));

        // Cleanup
        spyLogAppender.stop();
        spyLogAppender.clearEvents();
        logger.detachAppender(spyLogAppender);
    }

    @ParameterizedTest(name = "{1} customers with name \'{0}%\'")
    @CsvSource({"J, 2",
                "И, 3",
                "X, 0"})
    void findByNameStartsWithOrderByName_fetches_customers(String namePrefix, int count) {
        final var customers = customerRepository.findAllPaginated(
                CustomerSpecifications.nameStartsWith(namePrefix),
                PageRequest.of(0, 100)).toList();
        Assertions.assertNotNull(customers);
        Assertions.assertEquals(count, customers.size());
        Assumptions.assumingThat(count > 0, () -> {
            val name = customers.get(0).getName();
            Assertions.assertNotNull(name);
            Assertions.assertTrue(name.length() > 0);
        });
    }

    // Test a custom repository methods for Keyset Pagination

    @ParameterizedTest(name = "page {0} by {1} rows")
    @CsvSource({"0, 3, 3", "1, 3, 3", "0, 5, 5", "1, 5, 5", "2, 5, 2"})
    void findAllIds_is_correctly_paginated(int pageNumber, int pageSize, int expectedItems) {
        Pageable page = PageRequest.of(pageNumber, pageSize, Sort.by("name"));
        Page<Long> idsPage = customerRepository.findAllIds(Specification.where(null), page);
        Assertions.assertNotNull(idsPage);
        Assertions.assertEquals(expectedItems, idsPage.getNumberOfElements());
        idsPage.getContent().forEach(Assertions::assertNotNull);
        log.info("Customer ids, 2nd page loaded: " + idsPage.getContent() );

    }

    @ParameterizedTest(name = "filter by name \'{0}%\' page {1} by {2} rows has {3} items")
    @CsvSource({
            "D, 0, 3, 2",
            "D, 0, 10, 2",
            "И, 0, 5, 3",
            "И, 0, 2, 2"})
    void findAllIds_applies_specification_predicate(String namePrefix, int pageNumber, int pageSize, int expectedItems) {
        Pageable page = PageRequest.of(pageNumber, pageSize, Sort.by("name"));
        Specification<Customer> specification = CustomerSpecifications.nameStartsWith(namePrefix);
        Page<Long> idsPage = customerRepository.findAllIds(specification, page);
        Assertions.assertNotNull(idsPage);
        Assertions.assertEquals(expectedItems, idsPage.getNumberOfElements());
        idsPage.getContent().forEach(Assertions::assertNotNull);
    }

    @ParameterizedTest(name = "filter by name \'{0}%\' page {1} by {2} rows has {3} items")
    @CsvSource({
            "D, 0, 3, 2",
            "D, 0, 10, 2",
            "И, 0, 5, 3",
            "И, 0, 2, 2"})
    void findAll_is_correctly_paginated(String namePrefix, int pageNumber, int pageSize, int expectedItems) {
        Pageable page = PageRequest.of(pageNumber, pageSize, Sort.by("name"));
        Specification<Customer> specification = CustomerSpecifications.nameStartsWith(namePrefix);
        Page<Customer> customerPage = customerRepository.findAllPaginated(specification, page);
        Assertions.assertNotNull(customerPage);
        Assertions.assertEquals(expectedItems, customerPage.getNumberOfElements());
        customerPage.getContent().forEach(customer -> {
            Assertions.assertNotNull(customer);
            Assertions.assertNotNull(customer.getName());
            Assertions.assertTrue(customer.getName().startsWith(namePrefix));
        });

    }

}
