package tech.skai.springmvc.dao;

import lombok.extern.log4j.Log4j2;
import lombok.var;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.DisplayNameGenerator;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import tech.skai.springmvc.SpringMvcExamplesApplication;
import tech.skai.springmvc.dao.base.KeysetLimitingSpecificationExecutorImpl;
import tech.skai.springmvc.model.Customer;

import javax.persistence.Tuple;
import javax.persistence.TupleElement;
import java.util.*;
import java.util.function.BinaryOperator;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@DataJpaTest         // This loads just JPA layer
//@SpringBootTest    // This will load the whole Application Context
//@ContextConfiguration(classes = {SpringMvcExamplesApplication.class})
//@Transactional(propagation = Propagation.REQUIRED, readOnly = true, noRollbackFor = Exception.class)
@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
@Log4j2
class CustomerRepositoryCustomIT {
    @TestConfiguration
    @EnableJpaRepositories(   // (!) By default searches repository classes INSIDE this package
            basePackages = {"tech.skai.springmvc.dao"},
            repositoryBaseClass = KeysetLimitingSpecificationExecutorImpl.class
    )
    static class TestConfig {
    }


    @Autowired
    CustomerRepository customerRepository;

    public CustomerRepositoryCustomIT() {
    }

    @Test
    void findByMinRatingJpql_returns_filtered_entities() {
        // when
        final List<Customer> list = customerRepository.findByMinRatingJpql(100);

        // then
        Assertions.assertNotNull(list);
        final var names = list.stream().map(Customer::getRating).collect(Collectors.toList());
        log.info("Found cusomers with ratings: " + names);
        Assertions.assertFalse(list.isEmpty());
        Assertions.assertTrue( list.stream()
                .map(Customer::getRating)
                .allMatch(r -> r>=100));
    }

    @Test
    void findByMinRatingCq_is_equivalent_to_JPQL() {
        // when
        final List<Customer> listJpql = customerRepository.findByMinRatingJpql(100);
        final List<Customer> listCq = customerRepository.findByMinRatingCq(100);

        // then
        Assertions.assertNotNull(listJpql);
        Assertions.assertNotNull(listCq);
        Assertions.assertEquals(listJpql.size(), listCq.size());
        Assertions.assertTrue( IntStream.range(0, listCq.size())
                .allMatch(i -> listJpql.get(i).getId().equals(listCq.get(i).getId()))
        );
    }


    @Test
    void findWithOrdersCountCq_counts_orders() {
        // when
        final List<Tuple> listCq = customerRepository.findWithOrdersCountCq(100);
        // then
        Assertions.assertNotNull(listCq);
        final var tupleElementNames = listCq.get(0).getElements().stream()
                .map(TupleElement::getAlias)
                .collect(Collectors.toList());
        System.out.println("Tuple element aliases: " + tupleElementNames);
        listCq.stream()
                .peek(Assertions::assertNotNull)
                .map(Tuple::toArray)
                .map(Arrays::toString)
                .forEach(System.out::println);
    }

}
