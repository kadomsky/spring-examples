package tech.skai.springmvc.controller;

import lombok.extern.log4j.Log4j2;
import lombok.var;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.DisplayNameGenerator;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.http.HttpHeaders;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.RequestPostProcessor;
import org.springframework.util.Base64Utils;
import tech.skai.springmvc.configuration.security.WebSecurityConfig;
import tech.skai.springmvc.controller.model.assembler.CustomerModelAssembler;
import tech.skai.springmvc.controller.rest.CustomerResource;
import tech.skai.springmvc.dao.AddressRepository;
import tech.skai.springmvc.dao.CustomerRepository;
import tech.skai.springmvc.model.Customer;

import java.util.Optional;

import static org.hamcrest.Matchers.endsWith;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;
import static org.mockito.Mockito.only;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(controllers = {CustomerResource.class})     // This loads just web layer, and just 1 controller
@Import(CustomerModelAssembler.class)       // Controller dependency
//@SpringBootTest @AutoConfigureMockMvc     // This loads the whole Spring application context
@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
@Log4j2
class CustomerResourceTest {
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private CustomerRepository mockRepository;
    @MockBean
    private AddressRepository mockAddressRepository;

    /**
     * A post-processor that adds a basic Authorization header to the request.
     * Same as a standard post-processor: {@code SecurityMockMvcRequestPostProcessors.httpBasic("user", "pass")}.
     */
    static RequestPostProcessor authentication() {
        return request -> {
            request.addHeader(HttpHeaders.AUTHORIZATION, "Basic " + Base64Utils.encodeToString("api_user:api_pass".getBytes()));
            return request;
        };
    }


    @ParameterizedTest
    @CsvSource("15, John")
//    @WithMockUser(roles = "API_USER")           // Access rules depend only on the role (but can specify name/password too)
    void customer_valid_id_200(final long id, final String name) throws Exception {
        final var customer = new Customer();
        customer.setName(name);
        customer.setId(id);
        given(this.mockRepository.findById(id)).willReturn(Optional.of(customer));
        this.mockMvc
                .perform(
                    get("/api/customers/" + id)
//                        .with(authentication())               // Works with @SpringBootTest, but not with @WebMvcTest as it does not build security filters
//                        .with(SecurityMockMvcRequestPostProcessors.httpBasic("api_user", "api_pass"))     // equivalent to the previous
                          .with(user("test").password("test").roles("API_USER"))  // Access rules depend only on the role
                )
                .andDo(print())
                //.andDo(log())   // Only provides DEBUG log messages
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name").isString())
                .andExpect(jsonPath("$.name").value(name))
                .andExpect(jsonPath("$._links.self.href").isString())
                .andExpect(jsonPath("$._links.self.href").value(endsWith("/api/customers/" + id)));

        then(mockRepository).should(only()).findById(id);

        CustomerResourceTest obj = new CustomerResourceTest();

    }

    @ParameterizedTest(name = "with id={0} controller throws {1}")
    @CsvSource("0, javax.validation.ConstraintViolationException")   // TODO heck other exceptions
    @WithMockUser(roles = "API_USER")           // Access rules depend only on the role (but can specify name/password too)
    void customer_invalid_id_400(final int id, final String ExceptionClassName) throws Exception {
        mockMvc.perform(
                get("/api/customers/" + id))
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect( result -> Assertions.assertTrue(
                        Class.forName(ExceptionClassName).isInstance(result.getResolvedException())))
                .andExpect(result -> Assertions.assertTrue(
                        result.getResolvedException().getMessage().contains("id must be positive") ));

        then(mockRepository).shouldHaveNoInteractions();

    }


}
