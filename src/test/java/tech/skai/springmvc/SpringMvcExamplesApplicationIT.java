package tech.skai.springmvc;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;

@SpringBootTest
@ContextConfiguration(classes = {SpringMvcExamplesApplication.class})
class SpringMvcExamplesApplicationIT {

	@Test
	void contextLoads() {
	}

}
