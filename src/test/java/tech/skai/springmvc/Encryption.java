package tech.skai.springmvc;

import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.extern.log4j.Log4j2;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.util.Base64;

@Log4j2
class Encryption {

    private BCryptPasswordEncoder bCryptEncoder = new BCryptPasswordEncoder();

    public static @NonNull String base64Encode(@NonNull final String msg) {
        return Base64.getEncoder().encodeToString(msg.getBytes());
    }

    public static @NonNull String base64Decode(@NonNull final String msg) {
        return new String(Base64.getDecoder().decode(msg));
    }

    @Test
    void bCrypt_encoded_string_matches_the_original() {
        final String encoded = bCryptEncoder.encode("user:password");
        log.info(String.format("Encoded password: %s", encoded));
        Assertions.assertTrue(bCryptEncoder.matches("user:password", encoded));
    }

    @Test
    void base64_encoded_string_matches_the_original() {
        final String encoded = base64Encode("api_editor:api_editor_pass");
        log.info(String.format("Encoded password: %s", encoded));
        Assertions.assertEquals("api_editor:api_editor_pass", base64Decode(encoded));
    }

}
