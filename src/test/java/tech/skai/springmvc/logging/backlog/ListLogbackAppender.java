package tech.skai.springmvc.logging.backlog;

import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.AppenderBase;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ListLogbackAppender extends AppenderBase<ILoggingEvent> {

    private final List<ILoggingEvent> logs = Collections.synchronizedList(new ArrayList<>());;

    @Override
    protected void append(ILoggingEvent event) {
        logs.add(event);
    }

    public List<ILoggingEvent> getEvents() {
        synchronized (logs) {
            return new ArrayList<>(logs);
        }
    }

    public void clearEvents () {
        logs.clear();
    }

    public String getMessage (int index) {
        synchronized (logs) {
            if (logs.isEmpty()) {
                return null;
            }
            return logs.get(index).getMessage();
        }
    }
}
