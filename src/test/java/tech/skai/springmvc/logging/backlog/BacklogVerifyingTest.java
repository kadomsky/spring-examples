package tech.skai.springmvc.logging.backlog;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.Logger;
import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.Appender;
import lombok.extern.log4j.Log4j2;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.ArgumentMatchers.argThat;

@Log4j2
class ClassUnderTest {
    void methodUnderTest(String logLevel) {
        log.log(org.apache.logging.log4j.Level.toLevel(logLevel), "Test message");
    }
}

@ExtendWith(MockitoExtension.class)
class BacklogVerifyingTest {

    @Mock Appender<ILoggingEvent> mockLogAppender;
    @Captor ArgumentCaptor<ILoggingEvent> logCaptor;

    @AfterAll
    static void afterAll() {
        // Check that no test appenders remain attached to logger
        Logger logger = (Logger) LoggerFactory.getLogger(Logger.ROOT_LOGGER_NAME);
        List<String> names = new ArrayList<>();
        logger.iteratorForAppenders().forEachRemaining(appender -> names.add(appender.getName()));
        System.out.println("Remaining appenders for root logger: " + names);
    }

    @ParameterizedTest(name = "{0} event caught: {1}")
    @CsvSource({"INFO, true", "WARN, true", "ERROR, true"})
    void mockAppender_can_verify_logging_events(String logLevel, boolean shouldBeLogged) {
        // given
        Logger logger = (Logger) LoggerFactory.getLogger(Logger.ROOT_LOGGER_NAME);
                                // Can use getLogger(CLASS_UNDER_TEST) if logging is don exactly in that class, i.e.,
                                // not in its dependencies.
        logger.addAppender(mockLogAppender);

        // when
        new ClassUnderTest().methodUnderTest(logLevel);

        // then
        if (shouldBeLogged) {
            Mockito.verify(mockLogAppender, Mockito.times(1))
                    .doAppend(logCaptor.capture());
            Assertions.assertEquals(logLevel, logCaptor.getValue().getLevel().toString());
        } else {
            Mockito.verify(mockLogAppender, Mockito.never()).doAppend(
                    argThat(event -> event.getLevel()
                            .isGreaterOrEqual(Level.toLevel(logLevel))));
        }

        // cleanup
        logger.detachAppender(mockLogAppender);
    }


}
