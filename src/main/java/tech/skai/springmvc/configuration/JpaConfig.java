package tech.skai.springmvc.configuration;

import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import tech.skai.springmvc.dao.base.KeysetLimitingSpecificationExecutorImpl;

@Configuration
@EnableJpaRepositories(   // (!) By default searches repository classes INSIDE this package
        basePackages = {"tech.skai.springmvc.dao"},
        repositoryBaseClass = KeysetLimitingSpecificationExecutorImpl.class
)
public class JpaConfig {
}
