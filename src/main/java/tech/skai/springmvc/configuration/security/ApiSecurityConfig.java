package tech.skai.springmvc.configuration.security;

import org.springframework.boot.autoconfigure.security.SecurityProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.security.web.authentication.www.BasicAuthenticationEntryPoint;

// WebSecurityConfigurerAdapter defines an additional security filter chain along with its request matcher.

@Configuration
@Order(SecurityProperties.BASIC_AUTH_ORDER - 10)   // The order must be lower than that of the default fallback Security Filer Chain.
                                                    // Any filter chains that we want to be consulted before this one, must have still lower orders.
public class ApiSecurityConfig extends WebSecurityConfigurerAdapter {

    // Note: the first Matcher is used to decide whether to apply this filter chain to an HTTP request.
    //       Once the decision is made, no other filter chains are applied.
    //       Subsequent matchers only set the access rules to apply to different requests inside this filter chain.
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .antMatcher("/api/**")
                .cors()
                .and().csrf().disable()
                .authorizeRequests()
                .antMatchers(HttpMethod.POST).hasRole("API_EDITOR")
                .antMatchers(HttpMethod.PUT).hasRole("API_EDITOR")
                .antMatchers(HttpMethod.PATCH).hasRole("API_EDITOR")
                .antMatchers(HttpMethod.DELETE).hasRole("API_EDITOR")
                .anyRequest().hasRole("API_USER")
                .and()
                .httpBasic().authenticationEntryPoint(authenticationEntryPoint());
    }
    // TODO token-based authentication for the API.

    @Bean
    public AuthenticationEntryPoint authenticationEntryPoint(){
        BasicAuthenticationEntryPoint entryPoint =
                new BasicAuthenticationEntryPoint();
        entryPoint.setRealmName("api realm");
        return entryPoint;
    }

}