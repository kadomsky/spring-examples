package tech.skai.springmvc.configuration.security;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;

@Configuration
@EnableWebSecurity              // Automatically applies this class to the global Web Security
public class SecurityConfig {

    @Bean
    public UserDetailsService userDetailsService() {
        InMemoryUserDetailsManager manager = new InMemoryUserDetailsManager();
        manager.createUser(User
                .withUsername("api_user")
                .password(passwordEncoder().encode("api_pass"))
                .roles("API_USER").build());
        manager.createUser(User
                .withUsername("api_editor")
                .password(passwordEncoder().encode("api_editor_pass"))
                .roles("API_USER", "API_EDITOR").build());
        manager.createUser(User
                .withUsername("user")
                .password(passwordEncoder().encode("password"))
                .roles("USER").build());
        return manager;
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }
}
