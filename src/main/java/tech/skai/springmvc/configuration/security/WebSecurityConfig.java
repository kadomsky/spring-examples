package tech.skai.springmvc.configuration.security;

import org.springframework.boot.autoconfigure.security.SecurityProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

// WebSecurityConfigurerAdapter defines an additional security filter chain along with specific request matcher.

@Configuration
@Order(SecurityProperties.BASIC_AUTH_ORDER - 5)   // The order must be lower than that of the default fallback Security Filer Chain.
                                                  // Any filter chains that we want to be consulted before this one, must have still lower orders.
//@PropertySource(value = "classpath:config/security.yml", , factory = YamlPropertySourceFactory.class)
// - Imported in application.yml
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

// BASIC HTTP authentication

    // Note: the first Matcher is used to decide whether to apply this filter chain to an HTTP request.
    //       Once the decision is made, no other filter chains are applied.
    //       Subsequent matchers only set the access rules to apply to different requests inside this filter chain.
//    @Override
//    protected void configure(HttpSecurity http) throws Exception {
//        http
//                .antMatcher("/web/**")
//                .authorizeRequests()
//                //.antMatchers("/").permitAll() // permit everyone to the start page
//                //.anyRequest.authenticated()   // do not distinguish roles
//                .anyRequest().hasRole("USER")   // or just allow specific role
//                .and()
//                .formLogin().loginPage("/login").permitAll()
//                .and().logout().permitAll();
//    }


// OAuth2 authentication

    // Google OAuth2 authentication:
    // Client ID: 266411831169-iap72k81ri4hgaurif07745s9stk9us6.apps.googleusercontent.com
    // Client Secret: PNpRKpgIbFHvFeCYw7cw9rzg
    // Test user: cyril.kadomsky@gmail.com

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        // Note: if this filter chain is applied to "/web/**" path,
        //       then we also need to include paths that OAuth2 server uses: "/oauth2/**", "/login/**"
        http
                .requestMatchers()
                    .antMatchers("/web/**", "/static/**", "/oauth2/**", "/login/**")
                    .and()
                .authorizeRequests()
                    .anyRequest().authenticated()
                    .and()
                .oauth2Login();
    }

}