package tech.skai.springmvc.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;

@Entity
@Data
public class Address {
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Integer id;

    @NotBlank(message = "Please provide an address string")
    private String address;

    @JsonIgnore
    @Column(name = "customers_id", nullable = false, insertable=false, updatable=false)
    // This column is managed by OneToMany association, so it must NOT allow direst inserts/updates
    private Long customerId;

    public static Address of(String address) {
        Address addr = new Address();
        addr.setAddress(address);
        return addr;
    }

}
