package tech.skai.springmvc.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.var;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.PositiveOrZero;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "customers")
@NamedEntityGraphs( {
    @NamedEntityGraph(name = "Customer.addresses",
            attributeNodes = @NamedAttributeNode("addresses")
    )
})
@Data
public class Customer {
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Long id;

    @NotBlank(message = "Please provide a name")
    @Size(max=20, message = "Name is too long")
    private String name;
    private String phone;
    private String address;
    @PositiveOrZero(message = "Rating must be positive or zero")
    private Integer rating;

    // Unidirectional @OneToMany must specify @JoinColumn,
    // because otherwise JPA tries to use an intermediate table to implement the association.
    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.LAZY)
    @JoinColumn(name = "customers_id", nullable = false)   // nullable here must match the DB!
                // Otherwise JPA tries to set the FK with a separate query, and nonnull constraint gets violated
    private List<Address> addresses = new ArrayList<>();


    // Bi-directional @OneToMany association
    // The best way to map a @OneToMany association is to rely on the @ManyToOne side
    // to propagate all entity state changes
    @OneToMany(mappedBy = "customer",
            cascade = CascadeType.ALL,
            orphanRemoval = true,
            fetch = FetchType.LAZY
    )
    @JsonIgnore
    private List<Order> orders = new ArrayList<>();


    public static Customer of(String name, String phone, String address, Integer rating) {
        final Customer customer = new Customer();
        customer.setName(name);
        customer.setPhone(phone);
        customer.setAddress(address);
        customer.setRating(rating);
        return customer;
    }

    public Customer update(Customer other) {
        this.setName(other.getName());
        this.setPhone(other.getPhone());
        this.setAddress(other.getAddress());
        this.setRating(other.getRating());
        return this;
    }

    public boolean hasAddress(Address address) {
        if (address.getId() != null) {
            final var matches = getAddresses().stream()
                    .filter(a -> address.getId().equals(a.getId())).count();
            return matches > 0;
        }
        return false;
    }


    // Bidirectional association needs the parent entity to have utility methods to synchronize both sides.
    // You should always provide these methods whenever you are working with a bidirectional association
    // as, otherwise, you risk very subtle state propagation issues.

    public void addOrder(Order order) {
        orders.add(order);
        order.setCustomer(this);
    }

    public void removeOrder(Order order) {
        orders.remove(order);
        order.setCustomer(null);
    }


}
