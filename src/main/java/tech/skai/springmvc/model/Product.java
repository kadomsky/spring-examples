package tech.skai.springmvc.model;

import lombok.Data;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Objects;

@Entity
@Table(name = "products")
@Data
public class Product {
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Long id;
    @Lob
    @Column(name="description", length=1024, columnDefinition = "text")
    private String description;
    @Lob
    @Column(name="details", length=1024, columnDefinition = "text")
    private String details;
    private BigDecimal price;
    @Lob
    @Column(name="manual", columnDefinition = "mediumblob")
    private byte[] manual;

}
