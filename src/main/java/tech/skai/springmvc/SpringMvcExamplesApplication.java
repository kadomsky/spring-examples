package tech.skai.springmvc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import tech.skai.springmvc.dao.base.KeysetLimitingSpecificationExecutorImpl;

@SpringBootApplication
public class SpringMvcExamplesApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringMvcExamplesApplication.class, args);
	}

}
