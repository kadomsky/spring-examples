package tech.skai.springmvc.dao;

import org.springframework.data.jpa.repository.EntityGraph;
import tech.skai.springmvc.model.Customer;
import tech.skai.springmvc.model.Customer_;
import tech.skai.springmvc.model.Order;
import tech.skai.springmvc.model.Order_;

import javax.persistence.EntityManager;
import javax.persistence.Tuple;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.*;
import java.util.Collections;
import java.util.List;

public class CustomerRepositoryCustomImpl implements CustomerRepositoryCustom {
    private final EntityManager entityManager;

    public CustomerRepositoryCustomImpl(final EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    private final String SELECT_BY_MINIMAL_RATING =
            "select c from Customer c " +
                    "where c.rating >= :minRating " +
                    "order by c.rating asc";

    @Override
    @EntityGraph("Customer.addresses")
    public List<Customer> findByMinRatingJpql(final int minimalRating) {
        TypedQuery<Customer> query = entityManager.createQuery(SELECT_BY_MINIMAL_RATING, Customer.class);
        query.setParameter("minRating", minimalRating);
        query.setMaxResults(3);
        return query.getResultList();
    }

    @Override
    public List<Customer> findByMinRatingCq(final int minimalRating) {
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<Customer> cr = cb.createQuery(Customer.class);
        Root<Customer> customer = cr.from(Customer.class);
        cr.select(customer);
        ParameterExpression<Integer> minRatingParam = cb.parameter(Integer.class);
        cr.where(cb.ge(customer.get(Customer_.rating), minRatingParam));
        cr.orderBy(cb.asc(customer.get(Customer_.rating)));

        TypedQuery<Customer> query = entityManager.createQuery(cr);
        query.setParameter(minRatingParam, minimalRating);
        query.setMaxResults(3);
        return query.getResultList();
    }


    @Override
    public List<Tuple> findWithOrdersCountCq(int minimalOrderCount) {
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<Tuple> cr = cb.createTupleQuery();
        Root<Customer> customer = cr.from(Customer.class);
        //Fetch<Customer, Order> order = customer.fetch(Customer_.orders, JoinType.LEFT);
//        cr.multiselect(customer.get(Customer_.id), customer.get(Customer_.name));
        Join<Customer, Order> order = customer.join(Customer_.orders, JoinType.LEFT);
        cr.multiselect(customer.get(Customer_.id).alias("customer_id"),
                cb.sum(order.get(Order_.amount)).alias("sum_orders"));
        cr.groupBy(customer.get(Customer_.id));
        cr.having(cb.gt(cb.sum(order.get(Order_.amount)), 100));

        TypedQuery<Tuple> query = entityManager.createQuery(cr);
        query.setMaxResults(5);
        return query.getResultList();
    }

    @Override
    public List<Tuple> findWithOrdersCountJpql(int minimalOrderCount) {
        return Collections.emptyList();
    }

}