package tech.skai.springmvc.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import tech.skai.springmvc.model.Product;

import java.math.BigDecimal;

/**
 * Allows for easy CRUD operations, like {@code respository.findAll()} and {@code repository.findOne(id)}.
 * Any additional method, which has a name according to a standard pattern, will be provided by JPA.
 */
@Repository
public interface ProductRepository extends CrudRepository<Product, Long> {

    Product findFirstByDescriptionContaining(String substring);
    Product findAllByPriceBetween(BigDecimal from, BigDecimal to);


}

