package tech.skai.springmvc.dao;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Repository;
import tech.skai.springmvc.dao.base.KeysetLimitingSpecificationExecutor;
import tech.skai.springmvc.model.Customer;

import java.util.List;
import java.util.Optional;

/**
 * Allows for paging and sorting, as well as for easy CRUD operations,
 * like {@code respository.findAll()} and {@code repository.findOne(id)}.
 * Any additional method, which has a name according to a standard pattern, will be provided by JPA.
 */
@Repository
public interface CustomerRepository extends JpaRepository<Customer, Long>,
        KeysetLimitingSpecificationExecutor<Customer, Long>,  // keyset pagination methods
        CustomerRepositoryCustom {    // custom queries

    @EntityGraph("Customer.addresses")
    Optional<Customer> findById(Long id);

    // (!) HHH000104: firstResult/maxResults specified with collection fetch; applying in memory!
    // @EntityGraph("Customer.addresses")
    // Customer findFirstByPhone(String phone);

    /** Here paging is not applied, all relevant rows are loaded.
     * Specification must be a predicate that guaranties a limited number of results.
     **/
    @EntityGraph("Customer.addresses")
    List<Customer> findAll(Specification<Customer> specification);

    // (!) HHH000104: firstResult/maxResults specified with collection fetch; applying in memory!
    // This is because of OneToMany Associations.
    // We must use Keyset Pagination instead: findAllIds() and findAll() implemented in FindIdsSpecificationExecutorImpl.
    // @EntityGraph("Customer.addresses")
    // Page<Customer> findAll(Pageable pageable);

    // Applies Keyset pagination to guarantee that paging is done in DB rather then in memory.
    @EntityGraph("Customer.addresses")
    Page<Customer> findAllPaginated(Specification<Customer> specification, Pageable page);

    @EntityGraph("Customer.addresses")
    Optional<Customer> findFirst(Specification<Customer> specification, Sort sort);

    @EntityGraph("Customer.addresses")
    Optional<Customer> findFirstByPhone(String phone);

}

