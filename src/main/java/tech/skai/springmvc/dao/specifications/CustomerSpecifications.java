package tech.skai.springmvc.dao.specifications;

import org.springframework.data.jpa.domain.Specification;
import tech.skai.springmvc.model.Customer;
import tech.skai.springmvc.model.Customer_;

public final class CustomerSpecifications {

    public static Specification<Customer> nameStartsWith(final String prefix) {
        return (root, query, builder) ->
                builder.like(root.get(Customer_.name),
                        PredicateUtils.escapeAndWrapStringForStartsWith(prefix));
    }

    public static Specification<Customer> phoneEqual(final String phone) {
        return (root, query, builder) ->
                builder.equal(root.get(Customer_.phone), phone);
    }

}
