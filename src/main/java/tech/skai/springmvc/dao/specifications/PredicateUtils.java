package tech.skai.springmvc.dao.specifications;

public final class PredicateUtils {

    static String escapeAndWrapStringForStartsWith(final String value) {
        final String escaped = value.replace("%", "\\%").replace("_", "\\_");
        return escaped + "%";
    }

}
