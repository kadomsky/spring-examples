package tech.skai.springmvc.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import tech.skai.springmvc.model.Address;
import tech.skai.springmvc.model.Customer;

import java.util.List;
import java.util.Optional;

/**
 * Allows for easy CRUD operations, like {@code respository.findAll()} and {@code repository.findOne(id)}.
 * Any additional method, which has a name according to a standard pattern, will be provided by JPA.
 */
@Repository
public interface AddressRepository extends CrudRepository<Address, Integer> {

    Optional<Address> findFirstByAddress(String address);
    List<Address> findByAddressStartsWithOrderByAddress(String substring);

}

