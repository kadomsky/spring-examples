package tech.skai.springmvc.dao;

import tech.skai.springmvc.model.Customer;

import javax.persistence.Tuple;
import java.util.List;

/**
 * A fragment interface for the custom functionality for {@link Customer} repository.
 */
public interface CustomerRepositoryCustom {

    List<Customer> findByMinRatingJpql(int minimalRating);
    List<Customer> findByMinRatingCq(int minimalRating);


    List<Tuple> findWithOrdersCountJpql(int minimalOrderCount);
    List<Tuple> findWithOrdersCountCq(int minimalOrderCount);
}
