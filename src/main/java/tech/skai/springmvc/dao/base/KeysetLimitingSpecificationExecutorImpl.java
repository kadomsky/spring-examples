package tech.skai.springmvc.dao.base;

import lombok.extern.log4j.Log4j2;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.query.QueryUtils;
import org.springframework.data.jpa.repository.support.JpaEntityInformation;
import org.springframework.data.jpa.repository.support.SimpleJpaRepository;
import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.data.support.PageableExecutionUtils;
import org.springframework.lang.Nullable;
import org.springframework.util.CollectionUtils;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.*;
import java.io.Serializable;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

@NoRepositoryBean
@Log4j2
public class KeysetLimitingSpecificationExecutorImpl<E, ID extends Serializable> extends SimpleJpaRepository<E, ID>
        implements KeysetLimitingSpecificationExecutor<E, ID> {

    private final EntityManager entityManager;
    private final JpaEntityInformation<E, ID> entityInformation;

    public KeysetLimitingSpecificationExecutorImpl(final JpaEntityInformation<E, ID> entityInformation,
                                                   final EntityManager entityManager) {
        super(entityInformation, entityManager);
        this.entityManager = entityManager;
        this.entityInformation = entityInformation;
    }

    /**
     * Loads all entities that satisfy a given {@link org.springframework.data.jpa.domain.Specification} by pages.
     * To account for possible OneToMany associations, only ids are returned.
     */
    @Override
    public Page<ID> findAllIds(@Nullable Specification<E> specification, Pageable pageable) {
        TypedQuery<ID> query = this.getFindIdsTypedQuery(specification, pageable);
        return PageableExecutionUtils.getPage(query.getResultList(), pageable,
                () -> this.getCountQuery(specification, this.getDomainClass()).getSingleResult());
    }

    /**
     * Loads all entities that satisfy a given {@link org.springframework.data.jpa.domain.Specification} by pages.
     * This default implementation applies 'Keyset pagination', i.e. loads data with two queries:
     * first ids only, then full entities by the given ids list.
     */
    @Override
    public Page<E> findAllPaginated(final Specification<E> specification, final Pageable page) {
        final Page<ID> idsPage = this.findAllIds(specification, page);

        List<E> result;
        final List<ID> filmIds = idsPage.getContent();
        if (CollectionUtils.isEmpty(filmIds)) {
            result = Collections.emptyList();
        } else {
            // Retrieve films using IN predicate
            final Specification<E> idInSpecification = getIdInSpecification(filmIds);
            result = this.findAll(idInSpecification, page.getSort());
            // (!) Need to pass original Sort order again, because "id in list" condition does not preserves the order
        }
        return PageableExecutionUtils.getPage(result, page, idsPage::getTotalElements);
    }

    public Optional<ID> findFirstId(Specification<E> specification, Sort sort) {
        TypedQuery<ID> query = this.getFindIdsTypedQuery(specification,
                PageRequest.of(0, 1, sort));
        List<ID> results = query.getResultList();
        return results.isEmpty()? Optional.empty() : Optional.of(results.get(0));
    }

    /**
     * Loads the first entity satisfying the given {@link Specification} in the given {@link Sort} order.
     * If a single entity maps to several rows in sql result (because of associations),
     * this method guaranties that no extra rows are loaded.
     * This is achieved by 'Keyset pagination', i.e. data is loaded loaded with two queries:
     * first loads id only, then loads full entity with findById(id) method.
     */
    public Optional<E> findFirst(Specification<E> specification, Sort sort) {
        log.info("findFirst() fired!");
        Optional<ID> optionalId =  this.findFirstId(specification, sort);
        if (optionalId.isPresent()) {
            return this.findById(optionalId.get());
        }
        return Optional.empty();
    }

    private TypedQuery<ID> getFindIdsTypedQuery(final Specification<E> specification,
                                                final Pageable pageable) {
        final CriteriaBuilder criteriaBuilder = this.entityManager.getCriteriaBuilder();
        final CriteriaQuery<ID> criteriaQuery = criteriaBuilder.createQuery(this.entityInformation.getIdType());
        final Root<E> root = criteriaQuery.from(this.getDomainClass());

        // Get the entities ID only
        criteriaQuery.select((Path<ID>) root.get(this.entityInformation.getIdAttribute()));

        // Add filtering
        final Predicate predicate = specification.toPredicate(root, criteriaQuery, criteriaBuilder);
        if (predicate != null) {
            criteriaQuery.where(predicate);
        }

        // Add Sorting
        final Sort sort = pageable.getSort();
        if (sort!=null && sort.isSorted()) {
            criteriaQuery.orderBy(QueryUtils.toOrders(sort, root, criteriaBuilder));
        }

        final TypedQuery<ID> typedQuery = this.entityManager.createQuery(criteriaQuery);

        // Update Pagination attributes
        if (pageable.isPaged()) {
            typedQuery.setFirstResult((int) pageable.getOffset());
            typedQuery.setMaxResults(pageable.getPageSize());
        }

        return typedQuery;
    }


    /**
     * Provides a specification to filter an entity by the set of ids. Can be used with any entity
     * in {@link org.springframework.stereotype.Repository Repository}
     * methods that accept {@link org.springframework.data.jpa.domain.Specification Specification} filter parameter.
     */
    private Specification<E> getIdInSpecification(Collection<ID> filmIds) {
        if (CollectionUtils.isEmpty(filmIds)) {
            return null;
        }
        return (root, query, builder) -> root.get(this.entityInformation.getIdAttribute()).in(filmIds);
    }
}