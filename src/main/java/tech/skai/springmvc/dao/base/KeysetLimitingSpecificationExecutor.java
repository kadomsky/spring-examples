package tech.skai.springmvc.dao.base;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.lang.Nullable;

import java.io.Serializable;
import java.util.Optional;

/** This interface is used to add custom functionality to Spring Data JPA repositories.
 * @param <E> entity type.
 * @param <ID> id type.
 */
@NoRepositoryBean
public interface KeysetLimitingSpecificationExecutor<E, ID extends Serializable> extends JpaSpecificationExecutor<E> {

    /**
     * Loads all ids of entities that satisfy a given {@link Specification}, by pages.
     * Due to possible OneToMany associations, only ids are loaded and paginated.
     */
    Page<ID> findAllIds(@Nullable Specification<E> specification, Pageable pageable);

    /**
     * Loads all entities that satisfy a given {@link Specification} by pages.
     * If a single entity maps to several rows in sql result (because of associations),
     * this method guaranties that paging is done in DB rather then in memory, and no extra rows are loaded.
     * This is achieved by 'Keyset pagination', i.e. data is loaded loaded with two queries:
     * first loads ids only, then loads full entities with findAll(specification) by the given ids list.
     */
    Page<E> findAllPaginated(Specification<E> specification, Pageable page);

    /**
     * Returns the ID of the first entity that satisfies the given {@link Specification} in the given {@link Sort} order.
     * Due to possible OneToMany associations, only id is loaded.
     */
    Optional<ID> findFirstId(Specification<E> specification, Sort sort);

    /**
     * Loads the first entity satisfying the given {@link Specification} in the given {@link Sort} order.
     * If a single entity maps to several rows in sql result (because of associations),
     * this method guaranties that no extra rows are loaded.
     * This is achieved by 'Keyset pagination', i.e. data is loaded loaded with two queries:
     * first loads id only, then loads full entity with findById(id) method.
     */
    Optional<E> findFirst(Specification<E> specification, Sort sort);
}