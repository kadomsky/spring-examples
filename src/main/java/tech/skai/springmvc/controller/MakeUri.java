package tech.skai.springmvc.controller;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import org.springframework.web.servlet.mvc.method.annotation.MvcUriComponentsBuilder;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * Provides utility methods to build URIs under the Aggregate Root given by a MVC controller.
 */
@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
public class MakeUri {
    private final Class controllerClass;

    /**
     * Create an instance on MakeUri utility class, for a given Aggregate Root.
     * @param controllerClass controller class.
     * @return an instance of MakeUri
     */
    public static MakeUri of(@NotNull Class<?> controllerClass) {
        return new MakeUri(controllerClass);
    }

    /**
     * Build an URI for individual entity under the current Aggregate Root.
     * @param id Id of an entity
     * @return URI as string
     */
    public String from(@NotNull final Serializable id) {
        return MvcUriComponentsBuilder.fromController(controllerClass)
                .path("/{id}")
                .buildAndExpand(id)
                .toString();
    }

    /**
     * Build an URI for the current Aggregate Root.
     * @return URI as string
     */
    public String fromRoot() {
        return MvcUriComponentsBuilder.fromController(controllerClass)
                .build()
                .toString();
    }
}

