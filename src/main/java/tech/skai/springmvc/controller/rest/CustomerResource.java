package tech.skai.springmvc.controller.rest;

import lombok.extern.log4j.Log4j2;
import lombok.var;
import net.kaczmarzyk.spring.data.jpa.domain.GreaterThanOrEqual;
import net.kaczmarzyk.spring.data.jpa.domain.StartingWith;
import net.kaczmarzyk.spring.data.jpa.web.annotation.And;
import net.kaczmarzyk.spring.data.jpa.web.annotation.Spec;
import org.apache.logging.log4j.util.Strings;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.data.web.SortDefault;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import tech.skai.springmvc.controller.MakeUri;
import tech.skai.springmvc.controller.model.assembler.CustomerModelAssembler;
import tech.skai.springmvc.dao.CustomerRepository;
import tech.skai.springmvc.model.Address;
import tech.skai.springmvc.model.Customer;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

@RestController
@RequestMapping(value = "/api/customers", produces = "application/hal+json; charset=utf-8")
@Transactional
@Validated
@Log4j2
public class CustomerResource {

    private final CustomerRepository repository;
    private final CustomerModelAssembler assembler;
    private final PagedResourcesAssembler<Customer> pagedAssembler;

    public CustomerResource(CustomerRepository customerRepository,
                            CustomerModelAssembler customerAssembler,
                            @SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection") PagedResourcesAssembler<Customer> pagedAssembler) {
        this.repository = customerRepository;
        this.assembler = customerAssembler;
        this.pagedAssembler = pagedAssembler;
    }

    @GetMapping("/{id}")
    public EntityModel<Customer> get(
            @NotNull @Positive(message = "id must be positive") @PathVariable final Long id) {

        final var customer =  repository.findById(id)
                .orElseThrow( () -> new ResponseStatusException(
                        HttpStatus.NOT_FOUND,
                        String.format("Customer id=%d not found", id)) );
        return assembler.toModel(customer);
    }

    // 'Pageable' captures request parameters like: page, size, sort. Param names are set in app config.
    // 'Specification' can be created from http parameters with an open source Specification Argument Resolver.
    @GetMapping
    public CollectionModel<EntityModel<Customer>> all(
            @NotNull @And({
                    @Spec(path = "name", spec = StartingWith.class),
                    @Spec(path = "rating", params="minRating", defaultVal = "0", spec = GreaterThanOrEqual.class),
            }) final Specification<Customer> spec,
            @NotNull @SortDefault(sort = {"name"}, direction = Sort.Direction.ASC) final Pageable pageable) {

        log.info(String.format("PageRequest(%d, %d, %s)", pageable.getPageNumber(), pageable.getPageSize(), pageable.getSort()));
        log.info("    with specification: " + spec.toString());
        final Page<Customer> page = repository.findAllPaginated(spec, pageable);
        Link selfLink = Link.of(ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString());
        return pagedAssembler.toModel(page, assembler, selfLink);     // With HATEOAS pagination links
        // return assembler.toCollectionModel(list);        // Without them
    }

    @PostMapping(consumes = "application/json")
    @ResponseStatus(HttpStatus.CREATED)         // Only applies in successful scenario, when no exception was thrown
    public EntityModel<Customer> add(
            @Valid @RequestBody final Customer customer,
            final HttpServletResponse response) {

        if (Strings.isNotBlank(customer.getAddress()) && CollectionUtils.isEmpty(customer.getAddresses()) ) {
            customer.getAddresses().add(
                    Address.of(customer.getAddress()));
        }
        repository.save(customer);
        response.setHeader(
                HttpHeaders.LOCATION,
                MakeUri.of(CustomerResource.class).from(customer.getId()));
        return assembler.toModel(customer);
    }

    /**
     * Does not update the address list.
     */
    @PutMapping("/{id}")
    public EntityModel<Customer> save(
            @NotNull @Positive(message = "id must be positive") @PathVariable final Long id,
            @Valid @RequestBody final Customer customer,
            final HttpServletResponse response) {

        final Customer persistedCustomer = repository.findById(id)
                .orElseThrow( () -> new ResponseStatusException(
                        HttpStatus.NOT_FOUND,
                        String.format("Customer id=%d not found", customer.getId())
                ));
        persistedCustomer.update(customer);  // Does not copy the address list!
        if (Strings.isNotBlank(persistedCustomer.getAddress()) && CollectionUtils.isEmpty(persistedCustomer.getAddresses()) ) {
            persistedCustomer.getAddresses().add(
                    Address.of(persistedCustomer.getAddress()));
        }
        repository.save(persistedCustomer);
        response.setHeader(
                HttpHeaders.LOCATION,
                MakeUri.of(this.getClass()).from(id));
        return assembler.toModel(persistedCustomer);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@NotNull @Positive(message = "id must be positive") @PathVariable final Long id) {
        repository.deleteById(id);
    }

}
