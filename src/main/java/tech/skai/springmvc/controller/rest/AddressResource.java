package tech.skai.springmvc.controller.rest;

import lombok.extern.log4j.Log4j2;
import lombok.var;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import tech.skai.springmvc.controller.MakeUri;
import tech.skai.springmvc.controller.model.assembler.AddressModelAssembler;
import tech.skai.springmvc.dao.AddressRepository;
import tech.skai.springmvc.dao.CustomerRepository;
import tech.skai.springmvc.model.Address;
import tech.skai.springmvc.model.Customer;

import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

@RestController
@RequestMapping(value = "/api/customers/{customerId}/addresses", produces = "application/hal+json; charset=utf-8")
@Transactional
@Validated
@Log4j2
public class AddressResource {

    private final CustomerRepository customerRepository;
    private final AddressRepository repository;
    private final AddressModelAssembler assembler;

    public AddressResource(CustomerRepository customerRepository, AddressRepository addressRepository,
                           AddressModelAssembler addressAssemblerAssembler) {
        this.customerRepository = customerRepository;
        this.repository = addressRepository;
        this.assembler = addressAssemblerAssembler;
    }

    @GetMapping("/{id}")
    public EntityModel<Address> get(
            @NotNull @Positive(message = "customer id must be positive") @PathVariable final Long customerId,
            @NotNull @Positive(message = "id must be positive") @PathVariable final Integer id) {

        final var customer =  repository.findById(id)
                .orElseThrow( () -> new ResponseStatusException(
                        HttpStatus.NOT_FOUND,
                        String.format("Address id=%d not found", id)) );
        return assembler.toModel(customer);
    }

    @GetMapping
    public CollectionModel<EntityModel<Address>> all(
            @NotNull @Positive(message = "customer id must be positive") @PathVariable final Long customerId) {
        final var addresses =  customerRepository.findById(customerId)
                .map(Customer::getAddresses)
                .orElseThrow( () -> new ResponseStatusException(
                        HttpStatus.NOT_FOUND,
                        String.format("Customer id=%d not found", customerId)) );
        return assembler.toCollectionModel(addresses, customerId);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)     // Only applies in successful scenario, when no exception was thrown
    public EntityModel<Address> add(@NotNull @Positive(message = "id must be positive") @PathVariable final Long customerId,
                                    @NotBlank(message = "please provide an address line") @RequestBody final String addressLine,
                                    final HttpServletResponse response) {

        final Customer customer = customerRepository.findById(customerId)
                .orElseThrow( () -> new ResponseStatusException(
                        HttpStatus.NOT_FOUND,
                        String.format("Customer id=%d not found", customerId)) );

        Address address = repository.findFirstByAddress(addressLine)
                .map(addr -> {
                    if (!customer.hasAddress(addr)) {
                        throw new ResponseStatusException(
                                HttpStatus.CONFLICT,
                                String.format("The given address id=%d is already present for this customer id=%d not found", addr.getId(), customerId));
                    }
                    return addr;
                }).orElseGet(() -> Address.of(addressLine));
        customer.getAddresses().add(address);
        customerRepository.save(customer);

        response.setHeader(HttpHeaders.LOCATION,
                MakeUri.of(CustomerResource.class).from(customer.getId()));
        return assembler.toModel(address);
    }

    @PutMapping("/{id}")
    public EntityModel<Address> save(
            @NotNull @Positive(message = "id must be positive") @PathVariable final Integer id,
            @NotBlank(message = "please provide an address line") @RequestBody final String addressLine,
            final HttpServletResponse response) {

        final Address address = repository.findById(id)
                .orElseThrow(() ->
                        new ResponseStatusException(
                                HttpStatus.NOT_FOUND,
                                String.format("Address id=%d not found", id))
                );
        address.setAddress(addressLine);
        response.setHeader(HttpHeaders.LOCATION,
                MakeUri.of(CustomerResource.class).from(address.getCustomerId()));
        return assembler.toModel(
                repository.save(address)
        );
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(
            @NotNull @Positive(message = "id must be positive") @PathVariable final Long customerId,
            @NotNull @Positive(message = "id must be positive") @PathVariable final Integer id) {
        final Customer customer = customerRepository.findById(customerId)
                .orElseThrow( () -> new ResponseStatusException(
                        HttpStatus.NOT_FOUND,
                        String.format("Customer id=%d not found", customerId)) );
        if (customer.getAddresses().stream().map(Address::getId).noneMatch(id::equals)) {
            throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND,
                    String.format("Address id=%d not found for customer id=%d", id, customerId));
        }
        repository.deleteById(id);
    }



}
