package tech.skai.springmvc.controller;

import lombok.extern.log4j.Log4j2;
import lombok.var;
import net.kaczmarzyk.spring.data.jpa.domain.GreaterThanOrEqual;
import net.kaczmarzyk.spring.data.jpa.domain.StartingWith;
import net.kaczmarzyk.spring.data.jpa.web.annotation.And;
import net.kaczmarzyk.spring.data.jpa.web.annotation.Spec;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.web.SortDefault;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import tech.skai.springmvc.dao.AddressRepository;
import tech.skai.springmvc.dao.CustomerRepository;
import tech.skai.springmvc.model.Address;
import tech.skai.springmvc.model.Customer;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.PositiveOrZero;

@Controller
@RequestMapping(value = "/web/customers", produces = "text/html")
@Validated
@Transactional
@Log4j2
public class CustomerController {

    private CustomerRepository customerRepository;
    private AddressRepository addressRepository;

    @Autowired
    public CustomerController(CustomerRepository customerRepository, AddressRepository addressRepository) {
        this.customerRepository = customerRepository;
        this.addressRepository = addressRepository;
    }

    @GetMapping(path = {"/{id}"})
    public String customer(@Positive @PathVariable final Long id, final Model model) {
        final var customer = customerRepository.findById(id)
                .orElseThrow( () -> new ResponseStatusException(
                        HttpStatus.BAD_REQUEST,
                        String.format("Customer id=%d not found", id)
                ));

        // final var addresses = customer.getAddresses(); // Lazy associations only work inside @Transactional context
        model.addAttribute("customer", customer);      // but here all associations are eagerly fetched
        return "customer";
    }

    // 'Pageable' captures request parameters like: page, size, sort. Param names are set in app config.
    // 'Specification' can be created from http parameters with an open source Specification Argument Resolver.
    @GetMapping
    public String customers(
            @NotNull @And({
                @Spec(path = "name", spec = StartingWith.class),
                @Spec(path = "rating", params="minRating", defaultVal = "0", spec = GreaterThanOrEqual.class),
            }) final Specification<Customer> spec,
            @NotNull @SortDefault(sort = {"name"}, direction = Sort.Direction.ASC) final Pageable page,
            final Model model) {

        log.info(String.format("PageRequest(%d, %d, %s)", page.getPageNumber(), page.getPageSize(), page.getSort()));
        log.info("    with specification: " + spec.toString());

        model.addAttribute("customersPage", customerRepository.findAllPaginated(spec, page));
        return "customers";
    }

    @PostMapping
    public String customersAdd(@NotBlank @RequestParam final String name,
                               @RequestParam final String phone,
                               @PositiveOrZero @RequestParam final Integer rating,
                               @RequestParam(required = false) final String addressLine) {
        final var customer = new Customer();
        customer.setName(name);
        customer.setPhone(phone);
        customer.setRating(rating);
        customer.setAddress(addressLine);
        final var address = new Address();
        address.setAddress(addressLine);
        customer.getAddresses().add(address);
        customerRepository.save(customer);
        log.info(String.format("New customer saved with id %d", customer.getId()));
        log.info(String.format("New address saved with id %d", customer.getAddresses().get(0).getId()));

        return "redirect:/web/customers/" + customer.getId();
    }

    @PostMapping("/{id}/addresses")
    public String customerAddAddress(@Positive @PathVariable final Long id,
                                     @NotBlank @RequestParam final String addressLine) {

        final Address address = addressRepository.findFirstByAddress(addressLine)
                .orElseGet( () -> Address.of(addressLine) );
        return customerRepository.findById(id)
                .map(customer -> {
                    if (!customer.hasAddress(address)) {
                        customer.getAddresses().add(address);
                        customerRepository.save(customer);
                    }
                    return "redirect:/web/customers/" + customer.getId();
                })
                .orElse("redirect:/web/customers?sort=name,asc");
    }

}
