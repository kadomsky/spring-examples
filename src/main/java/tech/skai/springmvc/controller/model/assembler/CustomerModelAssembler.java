package tech.skai.springmvc.controller.model.assembler;

import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.server.RepresentationModelAssembler;
import org.springframework.stereotype.Component;
import tech.skai.springmvc.controller.rest.AddressResource;
import tech.skai.springmvc.controller.rest.CustomerResource;
import tech.skai.springmvc.model.Customer;

import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@Component
public class CustomerModelAssembler implements RepresentationModelAssembler<Customer, EntityModel<Customer>> {

    @Override
    public EntityModel<Customer> toModel(final Customer customer) {
        return EntityModel.of(customer, //
                linkTo(methodOn(CustomerResource.class).get(customer.getId())).withSelfRel(),
                linkTo(methodOn(CustomerResource.class).all(null, null)).withRel("customers"),
                linkTo(methodOn(AddressResource.class).all(customer.getId())).withRel("addresses"));
    }

    @Override
    public CollectionModel<EntityModel<Customer>> toCollectionModel(Iterable<? extends Customer> customers) {
        final Stream<EntityModel<Customer>> ems = StreamSupport.stream(customers.spliterator(), false)
                .map(this::toModel);
        return CollectionModel.of(
                ems::iterator,
                linkTo(methodOn(CustomerResource.class).all(null, null)).withSelfRel());
    }
}
