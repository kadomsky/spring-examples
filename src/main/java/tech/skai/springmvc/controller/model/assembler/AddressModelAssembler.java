package tech.skai.springmvc.controller.model.assembler;

import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.server.RepresentationModelAssembler;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;
import tech.skai.springmvc.controller.rest.AddressResource;
import tech.skai.springmvc.controller.rest.CustomerResource;
import tech.skai.springmvc.model.Address;

import java.util.Objects;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@Component
public class AddressModelAssembler implements RepresentationModelAssembler<Address, EntityModel<Address>> {

    @Override
    public EntityModel<Address> toModel(final Address address) {
        return EntityModel.of(address,
                linkTo(methodOn(AddressResource.class).get(address.getCustomerId(), address.getId())).withSelfRel(),
                linkTo(methodOn(AddressResource.class).all(address.getCustomerId())).withRel("siblings"));
    }

    @Override
    public CollectionModel<EntityModel<Address>> toCollectionModel(Iterable<? extends Address> addresses) {
        return toCollectionModel(addresses, null);
    }

    public CollectionModel<EntityModel<Address>> toCollectionModel(Iterable<? extends Address> addresses, @Nullable Long customerId) {
        final Stream<EntityModel<Address>> ems = StreamSupport.stream(addresses.spliterator(), false)
                .map(this::toModel);
        return Objects.nonNull(customerId)?
                CollectionModel.of(
                        ems::iterator,
                        linkTo(methodOn(AddressResource.class).all(customerId)).withSelfRel(),
                        linkTo(methodOn(CustomerResource.class).get(customerId)).withRel("customer")):
                CollectionModel.of(
                        ems::iterator);
    }
}
