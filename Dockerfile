# syntax=docker/dockerfile:1

# Image for sorce code
FROM openjdk:8u302-jdk AS base

RUN mkdir /usr/src/salesdept
WORKDIR /usr/src/salesdept

COPY .mvn/ .mvn
COPY mvnw pom.xml ./
RUN ./mvnw dependency:go-offline
RUN ./mvnw dependency:resolve-plugins

COPY src ./src


# Image for running tests
# With RUN, tests are done when image is build, and stop the build when they fail.
# Creating a container for tests is not necessary.
FROM base as test
ENV SPRING_PROFILES_ACTIVE test
RUN ["./mvnw", "test"]

# Image used for development. Runs an app at once and allows to connect a debugger from remote JVM.
FROM base as development
ENV SPRING_PROFILES_ACTIVE dev
CMD ["./mvnw", "spring-boot:run", "-DskipTests", "-Dspring-boot.run.jvmArguments='-agentlib:jdwp=transport=dt_socket,server=y,suspend=n,address=8000'"]



# Image for prod build. Just creates a final JAR.
FROM base as build
RUN ["./mvnw", "package", "-DskipTests"]


# Production image
FROM openjdk:8-jre-alpine as production
ENV SPRING_PROFILES_ACTIVE dev
EXPOSE 8080
RUN apk add --no-cache --upgrade bash
RUN mkdir /salesdept
WORKDIR /salesdept
COPY --from=build /usr/src/salesdept/target/spring-examples-*.jar ./spring-examples.jar
COPY image_resources .

#RUN groupadd -r springapp && useradd -r -s /sbin/false -g springapp springapp   # works in Debian
# (!) Alpine images do not include groupadd/useradd, and the commands for addgroup/adduser are slightly different:
RUN addgroup -S springapp && adduser -S --shell /sbin/false -G springapp springapp
RUN chown -R springapp:springapp /salesdept
USER springapp

CMD ["java", "-jar", "./spring-examples.jar"]
# 'exec' form of CMD is preferred as it run an executable without starting shell.