
INSERT INTO salesdept.customers (id, name, phone, address, rating) VALUES (12, 'ООО "Интрейд лтд"', '313-48-48', 'ул. Смольная, д. 7', 1000);
INSERT INTO salesdept.customers (id, name, phone, address, rating) VALUES (13, 'Иванов П.В.', '336-74-87', 'пр. Пушкина, 125', 3000);
INSERT INTO salesdept.customers (id, name, phone, address, rating) VALUES (14, 'Петров А.Б.', null, null, 266);
INSERT INTO salesdept.customers (id, name, phone, address, rating) VALUES (15, 'John', null, null, 100);
INSERT INTO salesdept.customers (id, name, phone, address, rating) VALUES (16, 'Jane', '987654321', null, 124);
INSERT INTO salesdept.customers (id, name, phone, address, rating) VALUES (22, 'Иван', '3343-22-22', 'Садовая, 14', 123);
INSERT INTO salesdept.customers (id, name, phone, address, rating) VALUES (23, 'Dan', '4774747', 'tututu', 11);
INSERT INTO salesdept.customers (id, name, phone, address, rating) VALUES (24, 'Danis', null, null, 13);
INSERT INTO salesdept.customers (id, name, phone, address, rating) VALUES (25, 'Иван Иванович', null, null, null);
INSERT INTO salesdept.customers (id, name, phone, address, rating) VALUES (26, 'Maria', '123456', null, 10);
INSERT INTO salesdept.customers (id, name, phone, address, rating) VALUES (43, 'NEW CUSTOMER', '12464772634', null, 0);
INSERT INTO salesdept.customers (id, name, phone, address, rating) VALUES (45, 'Fred', '236637462', 'Some address', 35);

INSERT INTO salesdept.address (id, address, customers_id) VALUES (1, 'The first address', 12);
INSERT INTO salesdept.address (id, address, customers_id) VALUES (2, 'The second address', 12);
INSERT INTO salesdept.address (id, address, customers_id) VALUES (3, 'The The first address', 13);
INSERT INTO salesdept.address (id, address, customers_id) VALUES (4, 'The The second address', 13);

INSERT INTO salesdept.products (id, description, details, price, manual) VALUES (1, 'Обогреватель Мосбытприбор ВГД 121R', 'Инфракрасный обогреватель. 3 режима нагрева: 400 Вт, 800 Вт, 1200 Вт', 1145.00, null);
INSERT INTO salesdept.products (id, description, details, price, manual) VALUES (2, 'Гриль Мосбытприбор СТ-14', 'Мощность 1440 Вт. Быстрый нагрев. Термостат.\\n		Цветовой индикатор работы', 2115.00, null);
INSERT INTO salesdept.products (id, description, details, price, manual) VALUES (4, 'Чайник Мосбытприбор МН', 'Цвет: белый. Мощность: 2200 Вт. Объем: 2 л', 925.00, null);
INSERT INTO salesdept.products (id, description, details, price, manual) VALUES (5, 'Утюг Мосбытприбор c паром АБ 200', 'Цвет: фиолетовый. Мощность: 1400 вт', 518.00, 0);

INSERT INTO salesdept.orders (id, date, product_id, qty, amount, customer_id) VALUES (1, '2007-12-12', 5, 8, 4500.00, 12);
INSERT INTO salesdept.orders (id, date, product_id, qty, amount, customer_id) VALUES (2, '2007-12-12', 2, 14, 22000.00, 12);
INSERT INTO salesdept.orders (id, date, product_id, qty, amount, customer_id) VALUES (3, '2008-01-21', 5, 12, 5750.00, 13);
INSERT INTO salesdept.orders (id, date, product_id, qty, amount, customer_id) VALUES (4, '2015-03-24', 4, 1, 1000.00, 14);
INSERT INTO salesdept.orders (id, date, product_id, qty, amount, customer_id) VALUES (6, null, 4, 1, 11.00, 12);
INSERT INTO salesdept.orders (id, date, product_id, qty, amount, customer_id) VALUES (7, '2005-11-11', 4, 1, 1000.00, 25);
INSERT INTO salesdept.orders (id, date, product_id, qty, amount, customer_id) VALUES (8, '2012-12-12', 1, 1000, 1000.00, 26);

