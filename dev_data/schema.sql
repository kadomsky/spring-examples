create schema salesdept;

create table customers
(
	id bigint unsigned auto_increment,
	name varchar(20) charset utf8 not null,
	phone varchar(20) charset utf8 null,
	address varchar(150) charset utf8 null,
	rating int null,
	constraint id
		unique (id),
	constraint index4
		unique (phone),
	constraint name_UNIQUE
		unique (name)
);

create index index3
	on customers (name);

alter table customers
	add primary key (id);

create table address
(
	id int unsigned auto_increment
		primary key,
	address varchar(45) charset utf8 not null,
	customers_id bigint unsigned not null,
	constraint fk_Address_customers1
		foreign key (customers_id) references customers (id)
);

create index fk_Address_customers1_idx
	on address (customers_id);

create table products
(
	id bigint unsigned auto_increment,
	description text charset utf8 null,
	details text charset utf8 null,
	price decimal(8,2) null,
	manual mediumblob null,
	constraint id
		unique (id)
);

alter table products
	add primary key (id);

create table orders
(
	id bigint unsigned auto_increment,
	date date null,
	product_id bigint unsigned not null,
	qty int unsigned null,
	amount decimal(10,2) null,
	customer_id bigint unsigned null,
	constraint id
		unique (id),
	constraint fkProduct
		foreign key (product_id) references products (id)
			on update cascade,
	constraint orders_ibfk_2
		foreign key (customer_id) references customers (id)
			on update cascade
);

create index customer_id
	on orders (customer_id);

create index fkProduct_idx
	on orders (product_id);

alter table orders
	add primary key (id);


