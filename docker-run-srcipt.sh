# This script is now replaced by Compose file

docker network create salesdept-app

# Run the DB server
docker run --rm -d `
--network salesdept-app --network-alias mysql `
--name dalesdept-mysql `
-v salesdept-mysql-data:/var/lib/mysql `
-v $PWD/dev_data/dump/:/docker-entrypoint-initdb.d `
-e MYSQL_ROOT_PASSWORD=secret_1234 `
-e MYSQL_USER=dbuser `
-e MYSQL_PASSWORD=secret_userpass `
-e MYSQL_DATABASE=salesdept `
mysql:5.7

# Run the main app after waiting for mysql response
docker run --rm -p 8080:8080 -p 8000:8000 `
--network salesdept-app `
--name dalesdept-app `
-e MYSQL_HOST=mysql `
-e MYSQL_USER=dbuser `
-e MYSQL_PASSWORD=secret_userpass `
-e MYSQL_DB=salesdept `
spring-examples:1.0
#./sh/wait-for-it.sh -t 0 mysql:3306 -- java -jar ./spring-examples-0.0.1-SNAPSHOT.jar
# optionally wait for DB container to become online before runing the app container.  

# Start the main app container with TTY attached without running an app
docker run --rm -it -p 8080:8080 -p 8000:8000 `
--network salesdept-app `
--name dalesdept-app `
-e MYSQL_HOST=mysql `
-e MYSQL_USER=dbuser `
-e MYSQL_PASSWORD=secret_userpass `
-e MYSQL_DB=salesdept `
spring-examples:1.0 `
/bin/bash


# Run unit tests
docker build -t spring-examples:1.0.tests . --target test
# Run the app for debug
docker-compose --profile debug up --build
# Run the app in production mode
docker-compose --profile run up --build
